# -*- coding: utf-8 -*-
"""
Created on Tue May  4 14:48:15 2021

@author: user
"""

# installing pyaudio in windows:
# https://stackoverflow.com/questions/52283840/i-cant-install-pyaudio-on-windows-how-to-solve-error-microsoft-visual-c-14
# for other python distributions (e.g. 3.8)
# https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio
# 
# for mac m1 - apple silicon
# https://stackoverflow.com/questions/68251169/unable-to-install-pyaudio-on-m1-mac-portaudio-already-installed
# install homebrew
# https://mac.install.guide/homebrew/index.html

import pyaudio
# import wave
import numpy as np
import matplotlib.pyplot as plt
from time import sleep
from threading import Thread

'''
# check in-out devices
p = pyaudio.PyAudio()
for i in range(p.get_device_count()):
    print(p.get_device_info_by_index(i))
# 'Mic/Inst/Line In 1/2 (Studio 18'
# put index in input_device_index=1,when initializing
# output = ...
'''

device_1_index = 0
device_2_index = -1

p = pyaudio.PyAudio()
for i in range(p.get_device_count()):
    d = p.get_device_info_by_index(i)
    print(d)
    if 'Mic/Inst/Line In 1/2 (Studio 18' in d['name'] and d['hostApi'] == 0:
        device_1_index = d['index']
    if 'Mic/Line In 3/4 (Studio 18' in d['name'] and d['hostApi'] == 0:
        device_2_index = d['index']

WINDOW_SIZE = 2048
CHANNELS = 1
RATE = 44100

FFT_FRAMES_IN_SPEC = 40

# global
# n = np.zeros(1)
global_block = np.zeros( WINDOW_SIZE*2 )
fft_frame = np.array( WINDOW_SIZE//2 )
win = np.hamming(WINDOW_SIZE)
spec_img = np.zeros( ( WINDOW_SIZE//2 , FFT_FRAMES_IN_SPEC ) )

user_terminated = False

#------------------------------------------------------------------------------------

# f = wave.open( 'audio_files/019.wav', 'rb' )


# %% call back with global

def callback( in_data, frame_count, time_info, status):
    global global_block, f, fft_frame, win, spec_img
    # global_block = f.readframes(WINDOW_SIZE)
    n = np.frombuffer( in_data , dtype='int16' )
    # begin with a zero buffer
    b = np.zeros( (n.size , CHANNELS) , dtype='int16' )
    # 0 is left, 1 is right speaker / channel
    # print(np.r_[ n[::2] , n[::2] ].shape)
    # pitch shift
    # b[:,0] = np.r_[ n[::2] , n[::2] ]
    b[:,0] = n
    # b[:,0] = 10000*np.random.rand(WINDOW_SIZE)
    # for plotting
    # audio_data = np.fromstring(in_data, dtype=np.float32)
    if len(win) == len(n):
        frame_fft = np.fft.fft( win*n )
        p = np.abs( frame_fft )*2/np.sum(win)
        # translate in dB
        fft_frame = 20*np.log10( p[ :WINDOW_SIZE//2 ] / 32678 )
        spec_img = np.roll( spec_img , -1 , axis=1 )
        spec_img[:,-1] = fft_frame[::-1]
    return (b, pyaudio.paContinue)

def user_input_function():
    k = input('press "s" to terminate (then press "Enter"): ')
    if k == 's' or k == 'S':
        global user_terminated
        user_terminated = True

# %% create output stream
p = pyaudio.PyAudio()

output1 = p.open(format=pyaudio.paInt16,
                channels=CHANNELS,
                rate=RATE,
                output=True,
                input=True,
                input_device_index=device_1_index,
                frames_per_buffer=WINDOW_SIZE,
                stream_callback=callback)


if device_2_index >= 0:
    output2 = p.open(format=pyaudio.paInt16,
                    channels=CHANNELS,
                    rate=RATE,
                    output=True,
                    input=True,
                    input_device_index=device_2_index,
                    frames_per_buffer=WINDOW_SIZE,
                    stream_callback=callback)


output1.start_stream()
if device_2_index >= 0:
    output2.start_stream()

threaded_input = Thread( target=user_input_function )
threaded_input.start()

# after starting, check when n empties (file ends) and stop
while output1.is_active() and not user_terminated:
    plt.clf()
    plt.imshow( spec_img[ WINDOW_SIZE//4: , : ] , aspect='auto' )
    # plt.axis([0,WINDOW_SIZE//8, -120,0])
    plt.show()
    plt.pause(0.01)

print('stopping audio')
output1.stop_stream()
if device_2_index >= 0:
    output2.stop_stream()
